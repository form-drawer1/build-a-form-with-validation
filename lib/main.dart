import 'package:email_validator/email_validator.dart';
import 'package:flutter/material.dart';

void main() {
  runApp(MyApp());
}

class MyApp extends StatelessWidget {
  const MyApp({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    const appTitle = 'Form Validation Demo';
    return MaterialApp(
      home: Scaffold(
        appBar: AppBar(
          title: const Text(appTitle),
        ),
        body: MyCustomFrom(),
      ),
    );
  }
}

class MyCustomFrom extends StatefulWidget {
  MyCustomFrom({Key? key}) : super(key: key);

  @override
  _MyCustomFromState createState() => _MyCustomFromState();
}

class _MyCustomFromState extends State<MyCustomFrom> {
  final _formKey = GlobalKey<FormState>();
  final double padding = 8.0;
  @override
  Widget build(BuildContext context) {
    return Form(
      key: _formKey,
      // autovalidateMode: AutovalidateMode.always,
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: <Widget>[
          Padding(
            padding: EdgeInsets.all(padding),
            child: TextFormField(
              decoration: InputDecoration(
                  labelText: 'Enter your email',
                  border: UnderlineInputBorder(),
                  helperText: 'example user@gmail.com'),
              validator: (value) {
                if (EmailValidator.validate(value!)) {
                  return null;
                }
                return 'Please enter email';
              },
            ),
          ),
          Padding(
            padding: EdgeInsets.all(padding),
            child: TextFormField(
              decoration: InputDecoration(
                hintText: 'Enter text',
                border: OutlineInputBorder(),
              ),
              validator: (value) {
                if (value == null || value.isEmpty) {
                  return 'Please enter some text';
                }
                return null;
              },
            ),
          ),
          Padding(
              padding: EdgeInsets.all(padding),
              child: SizedBox(
                width: double.infinity,
                height: 30.0,
                child: ElevatedButton(
                    onPressed: () {
                      if (_formKey.currentState!.validate()) {
                        ScaffoldMessenger.of(context).showSnackBar(
                            const SnackBar(content: Text('Processing Data')));
                      }
                    },
                    child: const Text('Submit')),
              ))
        ],
      ),
    );
  }
}
